const anagram = require('../anagram');

test('"bleat" and "table" are anagrams', () => {
    expect(anagram('bleat', 'table')).toBe(true);
});

test('"School master" and "The classroom" are anagrams', () => {
    expect(anagram('School master', 'The classroom')).toBe(true);
});

test('"Dormitory" and "Dirty room" are anagrams', () => {
    expect(anagram('Dormitory', 'Dirty room')).toBe(true);
});

test('"The eyes" and "They see" are anagrams', () => {
    expect(anagram('The eyes', 'They see')).toBe(true);
});

test('"user1234" and "23user14" are anagrams', () => {
    expect(anagram('user1234', '23user14')).toBe(true);
});

test('"Me" and "Them" are not anagrams', () => {
    expect(anagram('Me', 'Them')).toBe(false);
});

test('"Wild Bear" and "Polar Bear" are not anagrams', () => {
    expect(anagram('Wild Bear', 'Polar Bear')).toBe(false);
});

test('"user1234" and "3453user" are not anagrams', () => {
    expect(anagram('user1234', '3453user')).toBe(false);
});
