# README #

This readme file describes how to test the provided solutions.

>Please clone the repository and follow the next steps.

### Anagram

The code for the anagram finder solution is written in `anagram.js` file and can be tested by running:

```
npm test
```

A few testcases are written in `tests/anagram.test.js` file feel free to add more tests or edit the current ones to test the solution.

Please note that the tests were written using [jest](https://jestjs.io/docs/en/getting-started), so you might need to have it installed. 

### Video Player

To view the player simply open up the `video.html` file in your browser. (assuming you have cloned the repository)

---

Pseudo code for the functionalities to be added to the video player:

Play Pause:
```
IF videPaused OR videoEnded THEN
    playVideo
ELSE
    pauseVideo
ENDIF
```

Forward : (on user action)

```
SET videoCurrentTime to (videoCurrentTime + forward duration)
```
Rewind: 

```
SET videoCurrentTime to (videoCurrentTime - rewind duration)
```

Update Progressbar:
```
SET progressBarWidth to (videoCurrentTime / videoDuration) * 100
IF videEnded THEN 
    SET progressBarWidth to 0
ENDIF
```

### The Design Pattern
With these functions, I would go with the `Module Design Pattern` 
Since I am not going to export any modules to another one, I would try to 
wrap entire function in a IIFE and return nothing.

Not sure how much I can follow here though 

---
video credit goes to [coverr](https://coverr.co/)
