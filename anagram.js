function orderString(s) {
    // for ordering the strings to compare later;
    return s.split('').sort().join('');
}

function anagrams(first, second) {
    // removing non alphabetic characters and converting both string to lowercase;
    first = first.replace(/[^\w]/g, '').toLowerCase();
    second = second.replace(/[^\w]/g, '').toLowerCase();

    // now comparing the ordered version ov both strings would return our result;
    return orderString(first) === orderString(second);
}

module.exports = anagrams;
